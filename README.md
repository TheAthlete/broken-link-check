# Broken link checker #

Скрипт поиска битых ссылок. 

Перед использованием необходимо установить пакеты следующим образом

    $ cd broken-link-check/
    $ cpanm --installdeps .

Варианты запуска скрипта:

    $ perl bin/checker_with_graph.pl -u planetperl.ru -m 500 -c 5 \
      -g -f svg -o etc/panetperl_ru.svg -l "broken link check" -r "http_//planetperl.ru/"

    $ perl bin/checker_with_graph.pl -u habrahabr.ru -m 500 -c 5 \
      -g -f svg -o etc/habr_ru.svg -l "broken link check" -r "https_//habrahabr.ru/"

    $ perl bin/checker_with_graph.pl -u habrahabr.ru -m 100 -c 5 \
      -g -f png -o etc/habr_ru.png -l "broken link check" -r "https_//habrahabr.ru/"

    $ perl bin/checker_with_graph.pl -u habrahabr.ru -m 250 -c 5

Параметры запуска:

    --url | -u                  стартовая страница
    --max_urls | -m             максимальное количество урлов для скачивания
    --max_connects | -c         количество коннектов на хост
    --graphviz | -g             создать граф урлов
    --graphviz_log_level | -e   указать уровень логов при создании графа урлов, см. perldoc Log::Handler
    --format | -f               выходной формат файлов - png, svg, etc
    --output_file | -o          относительный путь до файла
    --label | -l                подпись графа
    --root | -r                 корневой узел для графа - т.к. используется драйвер twopi для создания радиального расположения графа

Также имется возможность управлять выводом логов с помощью переменной окружения PERL_ANYEVENT_VERBOSE, а именно

    $ export PERL_ANYEVENT_VERBOSE=n

где n:

- 5 (warn) - вывод ошибок http
- 6 (note) - детальный вывод ошибок http (ссылка на хэш $headers)
- 7 (info) - вывод трассировки вызовов к URLs
- 8 (debug) - вывод списка урлов, выкачанных со страницы
  
Скрипт собирает все ссылки в граф, на экран выводит код статуса и ссылку. 

Работает с использованием AnyEvent::HTTP, Web::Query, GraphViz2
