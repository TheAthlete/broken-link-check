use strict;
use warnings;
use feature 'say';

use Test::More;

use Data::Printer;

use Broken::Link::Check::URI;

my $uri = Broken::Link::Check::URI->new();

subtest 'canonical' => sub {
    my %urls = (
        'HTTP://WWW.perl.com:80' => 'http://www.perl.com/',
        'HTTP://WWW.perl.com:80/abc/def%3flang=ru' => 'http://www.perl.com/abc/def%3Flang=ru',
    );

    while (my ($raw_url, $normalized_url) = each %urls) {
        is $uri->canonical($raw_url), $normalized_url, "$raw_url is normalized";
    }
};

subtest 'path' => sub {
    my %urls = (
        'http://eax.me/products/teams/' => ['http://eax.me/', '/products/teams/'],
        'http://eax.me/hangouts%3Fn=1' => ['http://eax.me/', '/hangouts%3Fn=1'],
        # 'http://eax.me/support.twitter.com/articles/20170451' => ['http://eax.me/', '//support.twitter.com/articles/20170451'], # ???
    );

    while (my ($expected_url, $url_path) = each %urls) {
        # set path
        is $uri->path(@$url_path), $expected_url, "$url_path->[1] is added to $url_path->[0]";
        # get path
        is $uri->path($expected_url), $url_path->[1], "get $url_path->[1] from url $expected_url";
    }
};

subtest 'without_fragment' => sub {
    my %urls = (
        'http://eax.me/products/teams/#abcd' => 'http://eax.me/products/teams/',
    );

    while (my ($url_with_fragment, $url_without_fragment) = each %urls) {
        note "$url_with_fragment, $url_without_fragment";
        is $uri->without_fragment($url_with_fragment), $url_without_fragment, "deleted fragment from $url_with_fragment";
    }
    
};

# subtest 'with child' => sub {
#     
# };



done_testing();


# my $uri = Broken::Link::Check::URI->new();
# my $canonical = $uri->canonical('HTTP://EAX.me');
# say $canonical;
