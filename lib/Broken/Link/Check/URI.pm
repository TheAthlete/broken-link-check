package Broken::Link::Check::URI;

use strict;
use warnings;
use feature 'say';

our $VERSION = '0.01';

use Data::Printer;

use FindBin qw/$Bin/;

use URI;
use URI::Split qw(uri_split uri_join);
use URI::PathAbstract;
use URI::Escape;
use Domain::PublicSuffix;

sub new {
    my ($klass) = @_;
    return bless {
        suffix => Domain::PublicSuffix->new({
                data_file => "$Bin/../etc/effective_tld_names.dat"
        }),
    }, $klass;
}

sub canonical {
    my ($self, $url) = @_;
    return URI->new($url)->canonical->as_string;
}

sub path {  # get/set path
    my ($self, $url, $path) = @_;

    my $uri = URI::PathAbstract->new($url);

    return $uri->path unless $path;

    $uri->path($path);
    return $uri->as_string;
}

sub without_fragment {
    my ($self, $url) = @_;
    my $uri = URI->new($url);
    $uri->fragment(undef);
    return $uri->as_string
}

sub with_child {
    my ($self, $parent_url, $child_url) = @_;

    my $parent_uri = URI::PathAbstract->new($parent_url);

    my $uri_with_child = $parent_uri->parent->child($child_url);

    $child_url = $uri_with_child->as_string;

    return $child_url;

    # return URI::PathAbstract->new($parent_url)->parent->child($child_url)->as_string;
}

sub root_domain {
    my ($self, $url) = @_;
    return $self->{suffix}->get_root_domain( URI->new($url)->host );
}

sub normalize {
    my ($self, $parent_url, $child_url) = @_;

    my %parent_urls;
    @parent_urls{qw/scheme auth path query frag/} = uri_split($parent_url);

    my $parent_uri = URI::PathAbstract->new($parent_url);

    if (!$child_url) {
        if (! $parent_urls{scheme}) {
            $parent_url = uri_join('http', $parent_urls{path});
        } elsif ($parent_urls{scheme} ne 'http') {
            die "Warning! Supporting only 'http' scheme";
        }
        return $parent_url;
    } else {
        my %child_urls;
        @child_urls{qw/scheme auth path query frag/} = uri_split($child_url);

        if ($child_urls{path} =~ m{^/}i) {
            return uri_join($parent_urls{scheme}, $parent_urls{auth}, $child_urls{path}, $parent_urls{query}, $parent_urls{frag});
        } else {
            my $with_child = $parent_uri->parent->child($child_urls{path});
            return $with_child->as_string;
        }
    }
}


1;
