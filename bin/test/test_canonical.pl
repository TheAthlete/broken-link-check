#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';

use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use Broken::Link::Check::URI;

my $uri = Broken::Link::Check::URI->new();
my $canonical = $uri->canonical('HTTP://EAX.me');
say $canonical;
