#!/usr/bin/perl -w
use strict;
use warnings;

use utf8;
use feature 'say';

use AnyEvent;
use EV;
use AnyEvent::HTTP;

my $cv = AnyEvent->condvar;

http_get 'https://metacpan.org/module/AnyEvent::HTTP', sub {
   my ($body, $hdr) = @_;

   if ($hdr->{Status} =~ /^2/) {
     say 'Yes';
   } else {
     say "error, $hdr->{Status} $hdr->{Reason}";
   }
   $cv->send;

};

$cv->recv;
