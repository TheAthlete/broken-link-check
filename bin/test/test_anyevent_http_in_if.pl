#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';
use open qw/:std :utf8/;
use utf8;

use AnyEvent;
use EV;
use AnyEvent::HTTP;

use Data::Printer;

my @urls =  qw{http://eax.me/ https://adtoapp.com/};
my $x = 7;
my $cv = AnyEvent->condvar;

if ($x == 5) {
    $cv->begin;
    http_get $urls[0], sub {
      my ($content, $headers) = @_;
      say $urls[0];
      p $headers;
        
        $cv->end;
    };
    # say 'yes';
} else {
    $cv->begin;
    http_get $urls[1], sub {
      my ($content, $headers) = @_;
      say $urls[1];
      p $headers;
        
        $cv->end;
    };

}

$cv->recv;
