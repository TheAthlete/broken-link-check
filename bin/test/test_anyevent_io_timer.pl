#!/usr/bin/env perl -w

use strict;
use warnings;
use utf8;

use AnyEvent;

$| = 1; print "Enter text> ";

my $cv = AnyEvent->condvar;

my ($w, $t); 

$w = AnyEvent->io(
    fh => \*STDIN,
    poll => 'r',
    cb => sub {
        chomp(my $input = <STDIN>);
        warn "read: $input\n";
        undef $w;
        undef $t;
        $cv->send;
    }
);

$t = AnyEvent->timer(
    after => 4.5,
    cb => sub {
        if (defined $w) {
            warn "no input for a 4.5 sec\n";
            undef $w;
            undef $t;
        }
        $cv->send;
    }
);

$cv->recv;
