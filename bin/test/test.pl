#!/usr/bin/env perl
use strict;
use warnings;

use feature 'say';
use DDP;

my @array = (
    {  
        'favorite' => 'apples',  
        'second favorite' => 'oranges'  
    },  
    {  
        'favorite' => 'corn',  
        'second favorite' => 'peas',  
        'least favorite' => 'turnip'  
    },  
    {  
        'favorite' => 'chicken',  
        'second favorite' => 'beef'  
    });  

# my $hashRef = \@array;

# my @del_indexes =  reverse grep { $array[$_]->{favorite} ne 'chicken' } 0..$#array;
# p @del_indexes;

p @array;

# delete @array[0,1];
splice @array, $_, 1 for reverse grep { $array[$_]->{favorite} ne 'chicken' } 0..$#array;
# splice @array, 0, 2;
# splice @array, $_, 1 for @del_indexes;

# for my $site (@array){
# # for my $site (@{$hashRef}){
#     delete @{$hashRef}[$site] if ($site->{favorite} ne "chicken");
#     print "\n";
# }

# @$hashRef = grep $_->{favorite} eq 'chicken', @$hashRef;
# p $hashRef;

#
p @array;
