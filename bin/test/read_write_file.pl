#!/usr/bin/env perl 

use strict;
use warnings;

use feature 'say';
use DDP;

sub reader {
    my ($fn, $cb) = @_;
    open my $in, '<', $fn;
    while (my $ln = <$in>) {
        chomp $ln;
        $cb->($ln); # выполняем код для работы со строкой
    }
    close $in;
}

sub write_file {
    my ($fn, $cb) = @_;
    open my $out, '>', $fn;
    $cb->(sub { # передаем анонимную функцию для записи в файл
            my $ln = shift;
            syswrite($out, $ln.$/);
        });
    close $out;
}

write_file('./out.cvs', sub {
        my $writer = shift; # sub { my $ln = shift; syswrite() }
        reader('./in.csv', sub {
                my $ln = shift;
                my @fields = split /;/, $ln;
                return unless substr($fields[1], 0, 1) == 6;
                @fields = @fields[0,1,2];
                $writer->(join(';', @fields)); # вызываем анонимную функцию для записи в файл
            });
    });

# write_file('./out.cvs', subname write_file => sub {
#         my $writer = shift; # sub { my $ln = shift; syswrite() }
#         reader('./in.csv', subname reader => sub {
#                 my $ln = shift;
#                 my @fields = split /;/, $ln;
#                 return unless substr($fields[1], 0, 1) == 6;
#                 @fields = @fields[0,1,2];
#                 $writer->(join(';', @fields)); # вызываем анонимную функцию для записи в файл
#             });
#     });
