#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';
# use open qw/:std :utf8/;
# use utf8;

# use AnyEvent::Strict;
use AnyEvent;
# use EV;
use AnyEvent::Impl::Perl;
use AnyEvent::HTTP;
use Devel::Peek;

# use AnyEvent::Debug;
# my $shell = AnyEvent::Debug::shell 'unix/', '/home/theathlete/myshell';

use Data::Printer;
use Encode qw/decode encode/;

use constant {
    CONTENT => 0,
    HEADERS => 1,
};

my $url = 'http://dev-lab.info/2016/08/catalyst-sphinx-и-realtime-индекс/';
say $url;
say utf8::is_utf8($url);
Dump $url;
# $url = encode('UTF-8', $url);
# say $url;
# say utf8::is_utf8($url);
# Dump $url;


my $cv = AnyEvent->condvar;

http_head $url, sub {
# http_get $url, sub {
    my $real_url = $_[HEADERS]->{URL}; 
    say "$url -> $real_url";
    say utf8::is_utf8($url).' -> '.utf8::is_utf8($real_url);
    say 'headers: '.p($_[HEADERS]);
    # say 'content: '.p($_[CONTENT]);
    $cv->send;
};

$cv->recv;
