#!/usr/bin/perl -w
use strict;
use warnings;
use utf8;
use open qw/:std :utf8/;
use feature 'say';

use DDP;

use AnyEvent;
use AnyEvent::Handle;
 
my $cv = AnyEvent->condvar;

my $s = 'строка';
say "utf8: ".utf8::is_utf8($s);
 
my $hdl_read; $hdl_read = AnyEvent::Handle->new(
   fh => \*STDIN,
   on_error => sub {
      my ($hdl, $fatal, $msg) = @_;
      AE::log error => $msg;
      $hdl_read->destroy;
      $cv->send;
   },
   on_read => sub {
       my $self = shift;
       $self->unshift_read(
           line => sub {
               my ($hdl, $data) = @_;
               say "Привет: $data";
           });
       # p @_;
       $cv->send;
   },
);

# my $hdl_write; $hdl_write = AnyEvent::Handle->new(
#     fh => \*STDOUT,
#    on_error => sub {
#       my ($hdl, $fatal, $msg) = @_;
#       AE::log error => $msg;
#       $hdl_write->destroy;
#       $cv->send;
#    },
# );
 
# # send some request line
# $hdl_write->push_write ("getinfo\015\012");
#
# # read the response line
# $hdl_write->push_read (line => sub {
#    my ($hdl, $line) = @_;
#    say "got line <$line>";
#    $cv->send;
# });
 
$cv->recv;
