#!/usr/bin/env perl -w

use strict;
use warnings;

use feature 'say';
use DDP;

sub sub_factory {
    my $multiplier = shift;
    return sub {
        return shift() * $multiplier;
    }
}

my $double = sub_factory(2);
my $triple = sub_factory(3);

say '2 * 3 = '.$double->(3);
say '3 * 4 = '.$triple->(4);

sub do_action {
    my ($action, $value) = @_;
    return $action->($value);
}

say "do_action(double, 12) = ".do_action($double, 12);
say "do_action(triple, 7) = ".do_action($triple, 7);

sub create_summer {
    my $sum = 0;
    return sub {
        return( $sum += shift() );
    }
}

my $s1 = create_summer();
my $s2 = create_summer();

$s1->(1);
$s2->(2);
$s1->(3);
$s2->(4);
