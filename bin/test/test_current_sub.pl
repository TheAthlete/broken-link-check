#!/usr/bin/perl -w
use strict;
use warnings;

use feature qw/say state current_sub/;

use DDP;

sub closure {
    my $init = shift;

    return sub {
        state $counter = $init;
        say $counter;
        return if ++$counter > 10;
        __SUB__->();
    }
}

# my $cnt = closure(1);
# say $cnt->();
closure(1)->();
