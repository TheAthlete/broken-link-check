#!/usr/bin/env perl -w

use strict;
use warnings;
use utf8;
use feature qw/say state/;
use Getopt::Long;

# $timer = 'after' | 'interval'
GetOptions(
    "timer=s" => \my $timer,
) or die("Error in command line arguments\n");

use AnyEvent;

my $cv = AnyEvent->condvar;

if ($timer eq 'after') {
    my $w; $w = AnyEvent->timer(
        after => 1,
        cb => sub {
            undef $w;
            say "Fired after 1s";
            $cv->send;
        }
    );
} elsif ($timer eq 'interval') {
    my $p; $p = AnyEvent->timer(
        interval => 0.1,
        cb => sub {
            state $counter = 0;
            do { undef $p; $cv->send; } if ++$counter > 5;
            say "Fired $counter time";
        }
    );
}

$cv->recv;
