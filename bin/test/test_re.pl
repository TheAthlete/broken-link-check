#!/usr/bin/env perl 

use strict;
use warnings;

use feature 'say';

use DDP;

my %matches;

for my $cc (qw/--^ ^-^ -^ ^--^ ^- ^^/) {
 	my $re = qr/[$cc]/;
 	my $match = '';
 	do {$match .= chr($_) if chr($_) =~ $re} for (32..126); # printable ascii
	$matches{$cc} = $match;
}

p %matches;

