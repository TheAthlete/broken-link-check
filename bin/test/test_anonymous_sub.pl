#!/usr/bin/perl -w
use strict;
use warnings;

use feature 'say';
use DDP;

# Особенности анонимных функций
# 1. Динамическое создание
# 2. Ограниченное время жизни
# 3. Не видна снаружи
# 4. Диспетчерезация (аналог роутов в Dancer2/Mojolicious)
# 5. Можно передавать в качестве параметра

my $double = sub {
    my $val = shift;
    return $val * 2;
};

say "4 * 2 = ".$double->(4);

