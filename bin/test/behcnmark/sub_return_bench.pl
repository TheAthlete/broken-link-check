#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';

use Benchmark qw(:all);

my $time = 0.900905847549438;
cmpthese(timethese(20_000_000, {
    format_seconds => sub { format_seconds($time) },
    format_seconds_without_return => sub { format_seconds_without_return($time) },
    format_seconds_without_return_shift => sub { format_seconds_without_return_shift($time) },
    format_seconds_sprintf => sub { sprintf "%.4f", $time },
  }));

sub format_seconds {
    return sprintf "%.4f", shift;
}

sub format_seconds_without_return {
    sprintf "%.4f", $_[0];
}

sub format_seconds_without_return_shift {
    sprintf "%.4f", shift;
}
