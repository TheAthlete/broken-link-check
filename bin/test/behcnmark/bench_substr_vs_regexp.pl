#!/usr/bin/perl

use strict;
use warnings;
use feature 'say';

use DDP;
use Benchmark qw/:all/;

# my $fragment = '#name'; # URL fragment
# my %cmp_find_fragment = (
#     subst => sub { substr($fragment, 0, 1) eq '#'; },
#     regexp => sub { $fragment =~ /^#/; },
# );
# say 'cmp find fragment';
# cmpthese(5000_000, \%cmp_find_fragment);

# my $internal_path = '/contract';
# my %cmp_internal_path = (
#     subst => sub { substr($internal_path, 0, 1) eq "/" && (substr($internal_path, 1, 1) ne "/") },
#     regexp => sub { $internal_path =~ m{^/[^/].*} },
# );
# say 'cmp internal path';
# cmpthese(5000_000, \%cmp_internal_path);

# my $content_type = "text/html; charset=utf-8";
# my %cmp_content_type = (
#     regexp => sub { $content_type =~ m{^text/html} },
#     subst => sub { substr($content_type, 0, 9) eq 'text/html' },
# );
# say 'cmp content type';
# cmpthese(5000_000, \%cmp_content_type);

my $status = "504";
my %cmp_status = (
    regexp => sub { $status =~ m{^[45]} },
    regexp_full => sub { $status =~ m{^[45]\d\d$} },
    substr => sub { substr($status, 0, 1) eq '4' || substr($status, 0, 1) eq '5' },
    substr_with_var => sub { my $st = substr($status, 0, 1); $st eq '4' || $st eq '5' },
);
say 'cmp http status';
cmpthese(5000_000, \%cmp_status);

say "s vs tr";
my $STR = "$$-this and that";
timethese( 5000_000, {
        'sr'  => sub { my $str = $STR; $str =~ s/[aeiou]/x/g; return $str; },
        'tr'  => sub { my $str = $STR; $str =~ tr/aeiou/xxxxx/; return $str; },
    });

cmpthese( 5000_000, {
        'sr'  => sub { my $str = $STR; $str =~ s/[aeiou]/x/g; return $str; },
        'tr'  => sub { my $str = $STR; $str =~ tr/aeiou/xxxxx/; return $str; },
    });
