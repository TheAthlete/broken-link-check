#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';

use DDP;

use FindBin qw/$Bin/;
use lib "$Bin/../../lib";

use Broken::Link::Check::URI;

my $url = 'https://habrahabr.ru/post/320528/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag';
my $url_normalization = Broken::Link::Check::URI->new();
# p $url_normalization->{suffix};
p $url_normalization->{bin};
say $url_normalization->root_domain($url);
