#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';

use AnyEvent;

$| = 1; print "Enter your name> ";

my $cv = AnyEvent->condvar;

my $name;

my $wait_for_input = AnyEvent->io(
    fh => \*STDIN,
    poll => 'r',
    cb => sub {
        $name = <STDIN>;
        $cv->send;
    }
);

$cv->recv;

undef $wait_for_input; # watcher no longer needed

say "Your name is $name";
