#!/usr/bin/perl -w
use strict;
use warnings;
use feature 'say';
# use open qw/:std :utf8/;
# use utf8;

use AnyEvent;
use EV;
use AnyEvent::HTTP;
use AnyEvent::Log;
use Time::HiRes qw/time/;
# use List::Util qw/sum/;

# for debug
# use AnyEvent::Strict; # export PERL_ANYEVENT_STRICT=1
# use AnyEvent::Impl::Perl;

# TODO:
# Исправить периодически появляющуюся (плавающую) ошибку/варнинг
# Can't call method "_put_session" on an undefined value at /path/AnyEvent/Handle.pm line 2281 during global destruction.

# TODO: протестировать работу кэша на базе хэшей и на базе алгоритма 2Q, ARC, GClock, etc
# проверить на большой выборке (на большом сайте), насколько обычно заполняется perl-хэш и  кэш
# Register our DNS resolver as the default resolver
use AnyEvent::CacheDNS ':register';

use URI::Split qw(uri_join);
use URI::Escape;
# use Domain::PublicSuffix;
use Hash::Merge 'merge';
use Encode;
use Web::Query;
use Graph;
# use Getopt::Long ;
use Getopt::Long 'HelpMessage';


# use File::Spec;
use GraphViz2;
use Log::Handler;

use Data::Printer;

use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use constant {
    CONTENT => 0,
    HEADERS => 1,

    I => 0,
    ELEM => 1,

    CONTENT => 0,
    CONTENT_TYPE => 1,
};

use Broken::Link::Check::URI;
# use URL::Normalize;

=head1 NAME

    Broken link checker - скрипт проверки битых ссылок

=head1 SYNOPSIS

    --url,-u                  стартовая страница
    --max_urls,-m             максимальное количество урлов для скачивания
    --max_connects,-c         количество коннектов на хост
    --graphviz,-g             создать граф урлов
    --graphviz_log_level,-e   указать уровень логов при создании графа урлов, см. perldoc Log::Handler
    --format,-f               выходной формат файлов - png, svg, etc
    --output_file,-o          относительный путь до файла
    --label,-l                подпись графа
    --root,-r                 корневой узел для графа - т.к. используется драйвер twopi для создания радиального расположения графа
    --help,-h                 печатает данную справку

    Пример использования:

        perl bin/checker_with_graph.pl -u planetperl.ru -m 500 -c 5 -g -f svg -o etc/panetperl_ru.svg -l "broken link check" -r "http_//planetperl.ru/"
        perl bin/checker_with_graph.pl -u habrahabr.ru -m 500 -c 5 -g -f svg -o etc/habr_ru.svg -l "broken link check" -r "https_//habrahabr.ru/"
        perl bin/checker_with_graph.pl -u habrahabr.ru -m 100 -c 5 -g -f png -o etc/habr_ru.png -l "broken link check" -r "https_//habrahabr.ru/"
        perl bin/checker_with_graph.pl --url habrahabr.ru --max_urls 100 --max_connects 5 --graphviz --format png --output_file etc/habr_ru.png --label "broken link check" --root "https_//habrahabr.ru/"

=head1 VERSION

              0.01

=cut

GetOptions(
  "url|u=s"                 => \my $start_page_url, # стартовая страница
  "max_urls|m=i"            => \(my $cnt = 50), # максимальное количество урлов для скачивания
  "max_connects|c=i"        => \(my $max_connects = 4), # количество коннектов на хост
  "graphviz|g"              => \my $graphviz, # создать граф урлов
  "graphviz_log_level|e=s"  => \(my $graphviz_log_level = 'error'), # указать уровень логов при создании графа урлов, см. perldoc Log::Handler
  "format|f=s"              => \my $format, # выходной формат файлов - png, svg, etc
  "output_file|o=s"         => \my $output_file, # относительный путь до файла
  'label|l=s'               => \my $label, # подпись графа
  'root|r=s'                => \my $root, # корневой узел для графа - т.к. используется драйвер twopi для создания радиального расположения графа
                                          # данный корневой узел будет размещен в центре
  'request_time|t'          => \my $request_time, # вывести время запроса каждгого урла
  'help' => sub { HelpMessage(0) }
) or HelpMessage(1);

# die unless we got the mandatory argument
HelpMessage(1) unless $start_page_url;


my $url_normalization = Broken::Link::Check::URI->new();

# TODO:
# 1. Перевести с Web::Query на Mojo::DOM, HTML::Gumbo
# 2. Рассмотреть вариант перехода с AnyEvent::HTTP на Mojo::IOLoop, либо реализовать одним из модулей Promise/A+
# 3. Добавить DNS Cache
# 4. Использовать keep-alive соединения

# Нормализуем url для главной страницы
# $start_page_url = "http://$start_page_url" if $start_page_url !~ m{^http://}i;
# TODO: uri_join vs "http://$start_page_url"
$start_page_url = uri_join('http', $start_page_url) if $start_page_url !~ m{^http(s)?://}i;
$start_page_url = $url_normalization->canonical($start_page_url);

my $start_page_url_root = $url_normalization->root_domain($start_page_url);

# пары parent_url - current_url (current_url - url без учета редиректа, другими словами current_url - http://eax.me/devzen/, real_current_url - http://devzen.ru/)
my $g = Graph->new;

=head2 Структура данных с урлами для фетчинга

       $to_be_scanned = {
         parent_url => { 
             external_urls => {
               root_domain1 => [qw/url1 url2 url3/]
               root_domain2 => [qw/url1 url2 url3/]
             }
             internal_urls => [qw/url url url/],
         }
       }

  например, 

       $to_be_scanned = {
         'planetperl.ru' => { 
             external_urls => {
               'livejournal.com' => [qw{ 
                                         http://yuripats.livejournal.com/tag/perl
                                         http://syndicated.livejournal.com/planetperlru/profile
                                         http://dur-randir.livejournal.com/tag/perl
                                     }],
               'habrahabr.ru' => [qw/
                                     https://habrahabr.ru/post/320528/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag
                                     https://habrahabr.ru/post/304634/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag
                                     https://habrahabr.ru/post/313034/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag
                                  /]
             }
             internal_urls => [qw/
                                 http://blog.planetperl.ru/2009/10/post-1.html
                                 http://blog.planetperl.ru/2009/10/
                                 http://blog.planetperl.ru/alex-kapranoff/2009/09/
                                /],
         }
       }

=cut

my $to_be_scanned = {}; # to_be_scanned set

my %urls_with_redirects;
my %urls_with_errors;
my %http_params = (
    timeout => 30,
);
# my $count_tmp = 0;

# $AnyEvent::Log::FILTER->level('info');
$AnyEvent::HTTP::MAX_PER_HOST = $max_connects;

# TODO: 
# 1. переделать формат логирования, убрать лишнее
# 2. создать хэш $log => {warn => AnyEvent::Log, note => ..., }

# Для вывода ошибок http, установить уровень детализации логов в 5 (warn)
my $warn_log = AnyEvent::Log::logger(warn => \my $warn); # export PERL_ANYEVENT_VERBOSE=5

# Для детального вывода ошибок http (ссылка на хэш $headers), установить уровень детализации логов в 6 (note)
my $note_log = AnyEvent::Log::logger(note => \my $note); # export PERL_ANYEVENT_VERBOSE=6

# Для вывода трассировки вызовов к URLs, установить уровень детализации логов в 7 (info)
my $info_log = AnyEvent::Log::logger(info => \my $info); # export PERL_ANYEVENT_VERBOSE=7

# Для вывода списка урлов, выкачанных со страницы, установить уровень детализации логов в 8 (debug)
my $debug_log = AnyEvent::Log::logger(debug => \my $debug); # export PERL_ANYEVENT_VERBOSE=8

my @reqs_time;

# TODO: реализовать вариант с помощью Mojo::IOLoop и с помощью одного из модулей Promise/A+ (для AnyEvent и для Mojo::IOLoop)
# TODO: оформить в модуль
my $cv = AnyEvent->condvar;
my $req_time_begin = AnyEvent->time;
scan_website($cnt, sub { });
$cv->recv;

# TODO: реализовать вывод в лог, а не с помощью say
if ($request_time && @reqs_time) {
  my @times = ([ $reqs_time[0][0], $reqs_time[0][1] ]);
  for (1 .. $#reqs_time) {
      push @times, [
        $reqs_time[$_][0], # url
        sprintf("%.4f", ($reqs_time[$_][1] - $reqs_time[$_ - 1][1])), # request time
      ];
  }
  # say "sum: ".sum(map $_->[1], @times);
  say "Request time...";
  say "@$_" for sort {$a->[1] cmp $b->[1]} @times;
}


# TODO: http_get
# on_header => sub {
#     $_[0]{"content-type"} =~ /^text\/html\s*(?:;|$)/
# },

# TODO: реализовать сохранение графа на диск (сериализацию/десериализацию)
say "Количество вершин (урлов) в графе: ".scalar $g->vertices;

if ($graphviz) {
    my @E = $g->edges;

    my $logger = Log::Handler->new;

    $logger->add(screen => {
            maxlevel       => $graphviz_log_level, # debug
            message_layout => '%m',
            minlevel       => 'error',
        });

    my $graph = GraphViz2->new(
        edge   => { # ребро
            color => 'grey', 
            arrowsize => 0.7, 
            penwidth => 1.2, 
            weight => 0.1
        },
        global => {
            directed => 1, 
            driver => 'twopi'
        },
        graph  => {
            size => "50,50",
            # size => "7.75,10.25",
            # orientation => 'landscape',
            # ranksep => 5.0,
            # nodesep => 4.0,
            ranksep => 30.0, nodesep => 20.0,
            label => $label // '', 
            rankdir => 'LR', 
            fontname => 'Helvetica', 
            fontsize => 13,
            root => $root // '',
        },
        logger => $logger,
        node   => {shape => 'oval'}, # узел
    );

    my %edge_name_to_idx = (from => 0, to => 1);
    for my $node (@E) {
        my %edge;

        while (my ($name, $index) = each %edge_name_to_idx) {
            $edge{$name} = (
                exists $urls_with_redirects{$node->[$index]} 
                    ? "$node->[$index]\n->\n$urls_with_redirects{$node->[$index]}" 
                    : $node->[$index]
              ) =~ s/:/_/gr;
        }

        while (my ($name, $index) = each %edge_name_to_idx) {
            $graph->add_node(name => $edge{$name}, color => 'green') if exists $urls_with_redirects{$node->[$index]};
            if (exists $urls_with_errors{$node->[$index]}) {
                my $headers = $urls_with_errors{$node->[$index]};
                $edge{$name} .= ' -> '.$headers->{Status}.' -> '.$headers->{Reason};
                $graph->add_node(name => $edge{$name}, fontcolor => 'yellow', fillcolor => 'red', style => 'filled');
            }
        }

        $graph->add_edge(%edge) 
    }

    $graph->run(format => $format, output_file => $output_file);
}

=head1 Broken link checker

=head2 process_page

  process_page(current_page):
      for each link on the current_page: 
        if target_page is not already in your graph:
            create a Page object to represent target_page
            add it to to_be_scanned set
        add a link from current_page to target_page

=head2 scan_website

  scan_website(start_page)
      create Page object for start_page
      to_be_scanned = set(start_page)
      while to_be_scanned is not empty:
          current_page = to_be_scanned.pop()
          process_page(current_page)

=cut

sub scan_website {
  my ($count_url_limit, $cb) = @_;

  $to_be_scanned->{$start_page_url}{internal_urls} = [$start_page_url]; # to_be_scanned = set(start_page)

  my $do; $do = sub {
    my %urls; 

    # По сути берем $max_connects внутренних урлов и по одному внешнему и ложим в хэш массивов {parent_url1 => [child_url1, child_url2], parent_url2 => [...]}
    for my $parent_url (keys %$to_be_scanned) {
        my $type_urls = $to_be_scanned->{$parent_url}; # $type_urls - internal_urls|external_urls
        push @{$urls{$parent_url}}, splice(@{$type_urls->{internal_urls}}, 0, $max_connects);
        while (my ($root_domain, $external_urls) = each %{$type_urls->{external_urls}}) {
            push @{$urls{$parent_url}}, splice(@$external_urls, 0, 1);
        }
    }

    # Обрабатываем пачками по $max_connects внутренних урлов + по одному урлу из внешних сайтов
    process_page(\%urls, sub { 
      my $new_urls = shift;

      $to_be_scanned = merge($to_be_scanned, $new_urls) if $new_urls;

      if (scalar($g->vertices) >= $count_url_limit) {
          # TODO: разобраться, почему несколько раз вызывается  этот блок с undef $do
          # скорее всего потому, что process_page рекурсивно вызывается несколько раз, и пока не уничтожатся все ссылки, будет вывполнятся этот блок
        undef $do;
        $cb->(); # TODO: нужен ли $cb ?
        $cv->send;
      } 
      elsif (is_to_be_scanned($to_be_scanned)) {
        $do->();
      }

      });
  }; $do->();
}

sub process_page {
  my ($current_page_urls, $cb) = @_;

  # TODO: Подумать о создании своей очереди запросов (см. 43:40 https://www.youtube.com/watch?v=Sf1e2yOPbcM)
  $cv->begin;
  while (my ($parent_url, $current_urls) = each %$current_page_urls) {
      for my $current_url (@$current_urls) {

    # TODO: разобраться со случайной ошибкой:
    # 595 | http://planetperl.ru/ -> http://www.perl.org/
    # \ {
    #     Reason   "Connection timed out",
    #     Status   595,
    #     URL      "http://www.perl.org/"
    # }
    # запрос к http://www.perl.org/ падает с ошибкой "Connection time out"
    # причем в нормальной ситуации происходит редирект на https::/www.perl.org/
    
    $cv->begin;
    http_head $current_url, %http_params, sub {
      my $headers = $_[HEADERS];

      my $real_current_url = $headers->{URL};

      $info_log->(($parent_url // '')." -> $real_current_url") if $info;

      # для получения ссылок с учетом редиректа, запишем хэш $current_url -> $real_current_url;
      $urls_with_redirects{$current_url} = $real_current_url if $current_url ne $real_current_url;

      if ($headers->{Status} =~ /^[45]/ && !($headers->{Status} == 405 && $headers->{allow} =~ /\bget\b/i)) {
          $warn_log->("$headers->{Status} | $parent_url -> $real_current_url") if $warn;
          $note_log->(sub { p($headers) }) if $note;
          $urls_with_errors{$current_url} = $headers; # для вывода ошибок в граф
      } 
      elsif (
          ($start_page_url_root eq $url_normalization->root_domain($real_current_url)) # и сайт внутренний
          && (substr($headers->{'content-type'}, 0, 9) eq 'text/html') # и это веб-страница
      ) {
          $cv->begin;

          http_get $real_current_url, %http_params, sub {
              my ($content, $headers) = @_;

              my $req_time_end = AnyEvent->time;
              push @reqs_time, [
                $headers->{URL},
                sprintf("%.4f", ($req_time_end - $req_time_begin)),
                $req_time_begin,
                $req_time_end
              ];

              $content = content_decode($content, $headers->{'content-type'});

              my ($new_urls, $urls, %hrefs);

              # TODO: заменить Web::Query на Mojo::DOM
              # TODO: реализовать вариант с использованием Mojo::DOM и Web::Scraper, протестировать производительнось
              # TODO: возможно, тестировать и картинки (img)
              wq($content)->find('a')
              ->filter(sub {
                      my $href = $_[ELEM]->attr('href'); # TODO: shift->attr('href');
                      substr($href, 0, 1) ne '#' # если в большом содержании содержутся страницы с анкорами каждого раздела статьи, фильтруем их
                      && $href ne '/' 
                      && $href !~ m{^mailto:(?://)?[A-Z0-9+_.-]+@[A-Z0-9.-]+}i 
                      && ++$hrefs{$href} == 1 # для фильтрации уже существующих урлов
                      if $href 
                  })
              ->each(sub { # for each link on the current page
                      my $href = $_->attr('href');

                      # TODO: проверить зависимость нормализации от последовательности нормализации
                      # другими словами, можно ли перемещать строки с нормализацией без нарушения нормализации
                      $href = $url_normalization->canonical($href);
                      if (substr($href, 0, 1) eq "/" && substr($href, 1, 1) ne "/") { # если путь на сайте '/', '/contact' и не внешний (//dev.twitter.com/etc)
                          $href = $url_normalization->path($real_current_url, $href) ;
                      }
                      $href = $url_normalization->without_fragment($href);
                      # TODO: возможно, сохранять в граф урлы декодированные, т.е. вместо https_//ru.wikipedia.org/wiki/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%B4%D0%B2%D0%B8%D0%B3%D0%B0%D1%82%D0%B5%D0%BB%D1%8C_%D0%BF%D0%BE%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%BD%D0%BE%D0%B3%D0%BE_%D1%82%D0%BE%D0%BA%D0%B0
                      # сохранять https_//ru.wikipedia.org/wiki/Электродвигатель_постоянного_тока

                      # TODO: попробовать натравить на чекер битых ссылок pragmaticperl.com
                      # pragmaticperl.com отдает 404 на http head, хотя на http get норм работает

                      # $href = uri_unescape($href); 
                      # say ((++$count_tmp)." $real_current_url -> $href");
                      # $href = decode('UTF-8', uri_unescape($href));

                      unless($g->has_vertex($href)) { # if tarteg_page is not already in your graph

                          # TODO: посмотреть, почему blogspot.com не парсится с помощью root_domain
                          # т.е. http://catalystone.blogspot.ru/search/label/perl -> catalystone.blogspot.ru
                          # а должно blogspot.ru
                          my $root_domain = $url_normalization->root_domain($href) || 'fails';

                          $urls = ($start_page_url_root eq $root_domain) 
                            ? ($new_urls->{$real_current_url}{internal_urls} //= []) # если это первое обращение, то присваиваем пустой массив
                            : ($new_urls->{$real_current_url}{external_urls}{$root_domain} //= []);

                          push @$urls, $href; # add it to to_be_scanned set
                      }

                      # Записываем в граф пару real_current_url (parent) - href (current), т.к. будет хранить в графе не реальные (с учетом редиректа), а те, что на сайте (href)
                      $g->add_edge($real_current_url, $href);
                  });

              # Алгоритм работы такой:
              # 1) передаем $new_urls в колбек
              # 2) в колбеке делаем merge хэшей $to_be_scanned и $new_urls и записываем его в $to_be_scanned
              # 3) если лимит урлов исчерпали, выходим, иначе
              # 4) если если есть внутренние и внешние ссылки, то заново вызываем анонимную функцию $do->()
              # 5) заполняем хэш %urls = {parent_url => [qw/child_url1 child_url2 child_url3/]}:
              #     - $max_connects урлов из массива internal_urls
              #     - по одному урлу из массива external_urls для каждого корневого урла (пока один, возможно, нужно увеличить)
              # 6) ссылку на хэш передаем в process_page, 
              # 7) в process_page получаем этот хэш, получаем из него $parent_url и $current_url в цикле

              if (is_to_be_scanned($new_urls)) {
                  $debug_log->(($parent_url // '')." -> $real_current_url ".p($new_urls)) if $debug;
                  $cb->($new_urls);
              }

              $cv->end;

          };
      } 
      # Если в очередной пачке урлов имеются только внешние урлы, но при этом в хэше массивов $to_be_scanned еще остались записи, переходим сюда
      # Суть вот в чем:, допустим, есть некий агрегатор сайтов, например, planetperl.com, в котором практически все сайты - внешние,
      # причем для некоторых из них есть несколько урлов для одного корневого домена, т.е. есть страницы
      #               'habrahabr.ru' => [qw/
      #                                     https://habrahabr.ru/post/320528/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag
      #                                     https://habrahabr.ru/post/304634/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag
      #                                     https://habrahabr.ru/post/313034/?utm_source=habrahabr&utm_medium=rss&utm_campaign=tag
      #                                  /]
      # т.е. страницы разные, но корневой домен один. 
      # По алгоритму выборки урлов, мы для каждого корневого домена выбираем по одному урлу, в результате получаются все внешние, соответственно
      # выполняется только запрос HTTP HEAD. В результате, т.к. мы не получаем урлы со страниц и соответственно не вызываем колбек $cb->($new_urls) с новыми урлами
      # мы должны в этом случае вызвать колбек без параметров, чтобы продолжить обработку следущих урлов

      else {
          $cb->();
      }

      $cv->end;
    };
  }
  }
  $cv->end;
}

# TODO: см. IO::HTML
sub content_decode {
  my ($encoding) = $_[CONTENT_TYPE] =~ /charset=([-\w\d]+)/i;
  return decode($encoding || 'UTF-8', $_[CONTENT]);
}

sub is_to_be_scanned {
    my $urls = shift;

    my $is_to_be_scanned = 0;
    OUT: while (my ($parent_url, $type_urls) = each %$urls) {
        if (@$type_urls{internal_urls}) {
            $is_to_be_scanned = 1; 
            last;
        }
        while (my ($root_domain, $external_urls) = each %{$type_urls->{external_urls}}) {
            if (@$external_urls) {
                $is_to_be_scanned = 1; 
                last OUT; 
            }
        }
    }

    return $is_to_be_scanned;
}
